# Descripció del Projecte

Aquest repositori conté el codi i la configuració per a la Integració Contínua (CI) i el Desplegament Contínu (CD) d'un projecte Python desenvolupat pel Grup 54 de l'assignatura de Gestió de Projectes de Software (GPS). Aquesta plataforma està dissenyada per assegurar la qualitat del codi mitjançant tests automatitzats, anàlisi estàtica i altres pràctiques de desenvolupament.

---

## Components del Grup 54 (GPS)

- **Martí Palomares**
- **Pau Campillo**
- **Vladislav Lapin**
- **Mohamed Balich**
- **Zongjun Chen**

---

## Configuració del Repositori

### Estructura del Directori

- [**.gitlab-ci.yml:**](.gitlab-ci.yml) Fitxer de configuració del pipeline de CI/CD per a GitLab CI.
- [**README.md:**](README.md) Documentació del projecte.
- **practica-gps-ci:** Directori del projecte amb el codi font i altres recursos.
  - [**main.py:**](practica-gps-ci/main.py) Fitxer principal del codi.
  - [**requirements.txt:**](practica-gps-ci/requirements.txt) Fitxer que especifica les dependències del projecte.
  - [**test.py:**](practica-gps-ci/test.py) Fitxer de tests unitaris.
  - [**transform.py:**](practica-gps-ci/transform.py) Fitxer de funcions auxiliars.

### Configuració del Pipeline

- **Stages:**
  - [**test:**](.gitlab-ci.yml) Etapa per a l'execució de tests i anàlisi de codi.

- **Variables:**
  - [**PYTHON_VERSION:**](.gitlab-ci.yml) Versió de Python especificada per al projecte.

- **Before_script:**
  - [**Actualització del sistema i creació d'un entorn virtual.**](.gitlab-ci.yml)

- **Test:**
  - [**Activació de l'entorn virtual, instal·lació de dependències i execució de tests amb Pylint i Pytest.**](.gitlab-ci.yml)
  - [**Informe de cobertura amb Coverage.py.**](.gitlab-ci.yml)

### Suggeriments d'Utilització

1. **Entorn Virtual:**
   - Utilitza un entorn virtual per gestionar les dependències del projecte.

2. **Cobertura de Codificació:**
   - Executa els tests amb freqüència i mantén una bona cobertura de codificació.

3. **Informes de Cobertura:**
   - Consulta els informes de cobertura generats pel pipeline per identificar possibles àrees de millora.

---

**Nota:** Aquest repositori i la seva configuració de CI/CD estan destinats a ús acadèmic dins de l'assignatura de Gestió de Projectes de Software (GPS) i estan gestionats pel Grup 54. Per més informació, contacta amb els membres del grup o el professor de l'assignatura.
